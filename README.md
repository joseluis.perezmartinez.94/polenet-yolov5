# INSTALACIÓN

## 1. Instalar Docker

En este documento se recoge la instalación de Docker Engine para Ubuntu desde el repositorio oficial. Esta misma guía y otros métodos de instalación se pueden consultar en la [página web oficial](https://docs.docker.com/engine/install/ubuntu/). Para instalar Docker en otra distribución consultar [aquí](https://docs.docker.com/engine/install/).

### 1.1 Preparar el repositorio

1) Actualizar `apt`:

``` sh
sudo apt-get update

sudo apt-get install \
    ca-certificates \
    curl \
    gnupg \
    lsb-release
```

2) Añadir la clave GPG de Docker:

``` sh
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo gpg --dearmor -o /usr/share/keyrings/docker-archive-keyring.gpg
```

3) Configurar el repositorio estable:

``` sh
echo \
  "deb [arch=$(dpkg --print-architecture) signed-by=/usr/share/keyrings/docker-archive-keyring.gpg] https://download.docker.com/linux/ubuntu \
  $(lsb_release -cs) stable" | sudo tee /etc/apt/sources.list.d/docker.list > /dev/null
```

Para ver otras opciones de otros repositorios, consultar la [página oficial](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository).

### 1.2 Instalar Docker Engine

1) Actualizar el índice de paquetes de `apt` e instalar la última versión de Docker Engine:

``` sh
sudo apt-get update
sudo apt-get install docker-ce docker-ce-cli containerd.io
```

Para instalar una versión específica de Docker Engine consultar la [página oficial](https://docs.docker.com/engine/install/ubuntu/#install-using-the-repository).

2) Verificar que Docker Engine se ha instalado correctamente:

``` sh
sudo docker run hello-world
```

Este comando descarga una imagen de prueba y ejecuta el contenedor. Al ejecutarse, muestra el mensaje y finaliza.

3) Docker Engine ya está instalado. Se ha creado el grupo `docker`, pero no hay usuarios añadidos a él. Para ejecutar comandos Docker hay que usar `sudo` o añadir usuarios a ese grupo. Para realizar esta y otras configuraciones post-instalación, consultar [aquí](https://docs.docker.com/engine/install/linux-postinstall/).

## 2. Instalar Nvidia-Docker

Nvidia-Docker contiene los packetes de drivers, incluyendo CUDA, necesarios para poder utilizar la tarjeta gráfica desde Docker. Se va a cubrir la instalación para Ubuntu siguiendo las instrucciones de la [página oficial](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#docker) para Ubuntu.

### 2.1 Configurar Docker

``` sh
curl https://get.docker.com | sh \
  && sudo systemctl --now enable docker
```

### 2.2 Configurar el contenedor del Toolkit de NVIDIA

1) Configurar el repositorio estable y la clave GPG:

``` sh
distribution=$(. /etc/os-release;echo $ID$VERSION_ID) \
   && curl -s -L https://nvidia.github.io/nvidia-docker/gpgkey | sudo apt-key add - \
   && curl -s -L https://nvidia.github.io/nvidia-docker/$distribution/nvidia-docker.list | sudo tee /etc/apt/sources.list.d/nvidia-docker.list
```

Para ver otras opciones de otros repositorios, consultar la [página oficial](https://docs.nvidia.com/datacenter/cloud-native/container-toolkit/install-guide.html#setting-up-nvidia-container-toolkit).

2) Instalar el paquete `nvidia-docker2` (y sus dependencias):

``` sh
sudo apt-get update
sudo apt-get install -y nvidia-docker2
```

3) Reiniciar el daemon de Docker para completar la instalación:

``` sh
sudo systemctl restart docker
```

4) Comprobar que la instalación se ha completado con éxito:

``` sh
sudo docker run --rm --gpus all nvidia/cuda:11.0-base nvidia-smi
``` 

Si todo ha ido bien, se debería mostrar una salida similar a la siguiente:

``` sh
+-----------------------------------------------------------------------------+
| NVIDIA-SMI 450.51.06    Driver Version: 450.51.06    CUDA Version: 11.0     |
|-------------------------------+----------------------+----------------------+
| GPU  Name        Persistence-M| Bus-Id        Disp.A | Volatile Uncorr. ECC |
| Fan  Temp  Perf  Pwr:Usage/Cap|         Memory-Usage | GPU-Util  Compute M. |
|                               |                      |               MIG M. |
|===============================+======================+======================|
|   0  Tesla T4            On   | 00000000:00:1E.0 Off |                    0 |
| N/A   34C    P8     9W /  70W |      0MiB / 15109MiB |      0%      Default |
|                               |                      |                  N/A |
+-------------------------------+----------------------+----------------------+

+-----------------------------------------------------------------------------+
| Processes:                                                                  |
|  GPU   GI   CI        PID   Type   Process name                  GPU Memory |
|        ID   ID                                                   Usage      |
|=============================================================================|
|  No running processes found                                                 |
+-----------------------------------------------------------------------------+
```

## 3. Instalar Ultralitics YOLOv5

Ultralitics facilita imágenes Docker de sus implementaciones de YOLOv3 y YOLOv5 basados en [PyTorch](https://pytorch.org/). En esta guía se van a seguir las instrucciones de instalación de acuerdo a la [página oficial](https://docs.ultralytics.com/environments/Docker-Quickstart/).

### 3.1 Descargar la imagen de YOLOv5

La imagen de Docker está siempre actualizada con la última versión del [repositorio oficial](https://github.com/ultralytics/yolov5):

``` sh
sudo docker pull ultralytics/yolov5:latest
```

### 3.2 Crear un contenedor de YOLOv5

Para crear el contenedor de YOLOv5, se recomienda usar los siguientes flags:

* `-it`: Crea una instancia interactiva.
* `-v "path/to/local/folder:path/to/virtual/folder"`: Da acceso a una carpeta local. Típicamente querremos usar este comando para dar acceso a la carpeta con los dataset y/o guardar los resultados.
* `--gpus all`: Da acceso a las GPUs.

``` sh
sudo docker run --ipc=host -it -v ~/PROJ:/usr/src/app/PROJ --gpus all ultralytics/yolov5:latest
```

Adicionalmente se pueden usar otros flags Docker que se consideren convenientes. En este documento nos referiremos a la carpeta PROJ como la carpeta base en la que tendremos todos los ficheros del proyecto. El contenido y estructura exacta de este directorio se especificará en el apartado [siguiente](#4-carpeta-del-proyecto).

### Uso de YOLOv5

A partir de este punto ya nos encontraremos dentro del contenedor de YOLOv5. Se pueden ejecutar los comandos de YOLOv5 para entrenar y validar modelos o detectar objetos:

``` sh
$ python train.py  # train a model
$ python val.py --weights yolov5s.pt  # test a model for Precision, Recall and mAP
$ python detect.py --weights yolov5s.pt --source path/to/images  # run inference on images and videos
```

Se puede salir en cualquier momento de la sesión interactiva de Docker con `Ctrl + D` o el comando `exit` y se puede retomar la sesión con el comando `docker start`

```sh
sudo docker start <container_id> -i
```

## 4. Carpeta del proyecto

**WIP**

En el equipo se espera tener una carpeta base del proyecto a la que nos referiremos como `PROJ`. En esta carpeta se espera encontrar la carpeta `Tests`, con los distintos datasets sobre los que se va a trabajar. También se recomienda tener una carpeta `results` en la que colocar los resultados que se quieras extraer del contenedor Docker de YOLO5.

```
PROJ
 |
 - Tests
    |
    - 22009 Azh
    - Azahar 21334
    - Azahar_21181
    - ...
 - results
    |
    - ...
```

En este apartado tomaremos la carpeta PROJ como directorio raíz y trabajaremos con rutas relativas a éste.

### 4.1 Clonar el repositorio `polenet-yolov5`

Todos los ficheros desarrollados para trabajar con el dataset de Polenet con YOLOv5 se encuentran en el repositorio de GITLAB [`polenet-yolov5`](https://gitlab.com/joseluis.perezmartinez.94/polenet-yolov5).

Para trabajar con el repositorio, primero hay que instalar GIT. Esto se puede hacer con el comando:

``` sh
sudo apt-get install git-all
```

Una vez tenemos instalado GIT, podemos clonar el repositorio ejecutando desde PROJ el comando:

``` sh
git clone https://gitlab.com/joseluis.perezmartinez.94/polenet-yolov5.git
```

**Nota**: Este comando solicitará las credenciales de la cuenta de GITLAB. Se puede crear una cuenta gratuitamente en la [página web](https://gitlab.com/users/sign_in). Adicionalmente, puede que este paso falle si no se tienen los permisos necesarios. Si esto ocurre, se puede contactar conmigo (<jopem13@etsid.upv.es>) para que revise y actualice los permisos.

Tras clonar el repositorio, el directorio incluirá ahora la carpeta `polenet-yolov5`:

```
PROJ
 |
 - Tests
    |
    - 22009 Azh
    - Azahar 21334
    - Azahar_21181
    - ...
 - results
    |
    - ...
 - polenet-yolov5
    |
    - README.md
    - requirements.txt
    - gen_conf_mat.py
    - gen_labels.py
    - utils.py
```

### 4.2 Preparar el Entorno Virtual

Para trabajar con los scripts de python, se necesita tener instalado [Python 3](https://www.python.org/downloads/) (se recomienda Python 3.8 o superior) y PIP.

``` sh
sudo apt install python3.8
sudo apt install -y python3-pip
```

Se recomienda trabajar en un entorno virtual de Python para evitar generar conflictos con otros proyectos que requieran versiones distintas de los módulos que se emplearan. Para instalar el gestor del entorno virtual:

``` sh
pip3 install virtualenv
```

Una vez instalado, se puede crear el entorno virtual con el comando:

``` sh
python3 -m venv my_env
```

Esto creará en nuestro entorno de trabajo el directorio `my_env`, donde se guarda la configuración del entorno virtual. El entorno virtual trabajará con la misma versión de Python que se usó para crearlo. Para activar *de facto* el entorno hay que ejecutar el siguiente comando:

``` sh
source my_env/bin/activate
```

Sabremos que estamos en el entorno virtual porque aparecerá el nombre del entorno en la consola:

```
(my_env) [user@localhost] $
```

Una vez en el entorno virtual, instalamos las dependencias de los scripts recogidas en `requirements.txt`:

``` sh
pip install -r requirements.txt
```

Para salir del entorno virtual cuando se finalice el trabajo ejecutar:

``` sh
deactivate
```

### 4.3 Uso de `gen_labels.py`

**WIP**

Para poder ejecutar YOLOv5 con nuestro dataset de imágenes necesitaremos preparar las etiquetas de las imágenes en el formato usado por YOLO. Las etiquetas deben estar en un fichero `.txt` por imagen, con el mismo nombre que la imagen a la que hace referencia. YOLOv5 buscará el fichero de etiquetas de una imagen en un fichero con el mismo path y el mimo nombre que la imagen original, reemplazando la primera occurencia de `images` por `labels` y la terminación (`.jpg`, `.png`, etc.) por `.txt`:

```
./rel/path/to/datasets/img_01.jpg               ->  ./rel/path/to/datasets/img_01.txt
/path/to/my/images/img_02.jpg                   ->  /path/to/my/labels/img_02.txt
/path/with/images/and/more/images/img_03.jpg    ->  /path/with/labels/and/more/images/img_03.txt

```

El fichero de etiquetas debe contener una etiqueta por línea, cada línea con el siguiente formato:

```
<class> <center_x> <center_y> <bb_heigh> <bb_width>
```

* `<class>`: ID de la clase. Las clases deben estar numeradas empezando por el 0.
* `<center_x>`: Coordenada X del centro del Bounding Box. Debe estar normalizado (i.e.: dividido por el ancho de la imagen).
* `<center_y>`: Coordenada Y del centro del Bounding Box. Debe estar normalizado (i.e.: dividido por el alto de la imagen).
* `<bb_heigh>`: Ancho del Bounding Box. Debe estar normalizado (i.e.: dividido por el ancho de la imagen).
* `<bb_width>`: Alto del Bounding Box. Debe estar normalizado (i.e.: dividido por el alto de la imagen).

Adicionalmente es necesario personalizar el fichero de configuración de YOLOv5 y el fichero con información del dataset. También es recomendable preparar un fichero `train.txt` y un fichero `test.txt` para gestionar más cómodamente qué imágenes del dataset se usarán para entrenamiento y qué imágenes se reservarán para validar el modelo una vez entrenado.

Para facilitar la generación de todos estos ficheros se facilita el scripts `gen_labels.py`:

```
usage: gen_labels.py [-h] -d XML [XML ...] [-C CLASS [CLASS ...]] [-S SIZE]
                     [-l] [-c] [-t [RATIO]] [-s [sorted]]

Polenet Dataset Manager for YOLOv5

This script offers a set of functionalities for managing and preparing the
Polenet dataset for working with YOLOv5.

optional arguments:
  -h, --help            show this help message and exit
  -d XML [XML ...], --datasets XML [XML ...]
                        One or more XML files with the description of a Polenet dataset.
  -C CLASS [CLASS ...], --classes CLASS [CLASS ...]
                        Polenet classes (ID) to be used in the model.
  -S SIZE, --cnn-size SIZE
                        Polenet classes (ID) to be used in the model.
  -l, --gen-labels      Generates YOLOv5 labels for the provided classes from the selected datasets. If no classes are provided explicitely, all the classes found on the datasets will be used.
  -c, --gen-config      Generates YOLOv5 labels for the provided classes from the selected datasets. If no classes are provided explicitely, all the classes found on the datasets will be used.
  -t [RATIO], --train-test [RATIO]
                        Generates a "train.txt" and a "test.txt" files. The images from Polenet are distributed between these two files. The ratio of images provided to each set is defined by the parameter RATIO, where:
                         - num_images * RATIO images are assigned to "train.txt"
                         - num_images * (1-RATIO)  images are assigned to "test.txt"
                        RATIO is expected in the range [0, 1]. By default RATIO = 0.9.
  -s [sorted], --show [sorted]
                        Show a summary of the labels at the provided Polenet datasets. If specified "sorted", the labels will be orderer by ocurrence.
```

Tras proveer al script con uno o más fichero XML de datasets de Polenet, permite realizar las siguientes acciones:

1) Mostrar las etiquetas registradas en el/los XML:

``` sh
python gen_labels.py -d <fichero_xml> [<otro_fichero_xml> ...] -s
```

También puede ordenar la lista de etiquetas por el número de apariciones de cada una:

``` sh
python gen_labels.py -d <fichero_xml> -s sorted
```

2) Generar las etiquetas de las imagenes con el formato YOLO.

``` sh
python gen_labels.py -d <fichero_xml> -l
```

Esto generará las etiquetas de las imágenes en el mismo directorio en el que se encuentran estas. Por defecto genera las etiquetas para todas las clases. Para seleccionar un subconjunto de clases se puede indicar mediante el parámetro `-C`:

``` sh
python gen_labels.py  -d <fichero_xml> -C 7 12 17 -l
```

3) Generar los ficheros con `train.txt` y `test.txt` con los sets de imagenes a usar en el entrenamiento y para la validación, respectivamente.

``` sh
python gen_labels.py -d <fichero_xml> -t
```

Se generan dos sets disjuntos a partir de las imagenes disponibles. Solo se asignan imágenes que tengas un fichero de etiquetas generado. Por defecto se asignan en un ratio de 90% entrenamiento y 10% validación. Se puede modificar el ratio de asignación especificándolo tras el parámetro `-t`:

``` sh
python gen_labels.py -d <fichero_xml> -t 0.8    # 80% entrenamiento, 20% validación
```

4) Generar los ficheros de configuración y del dataset:

``` sh
python gen_labels.py  -d <fichero_xml> -c
```

Esto generará los ficheros `polenet-TOLOv5l.yaml` y `polenet.yaml`, que son respectivamente el fichero de configuración y el de información del dataset.

**Nota**: El fichero de información del dataset generado (`polenet.yaml`) espera que los sets de imagenes a utilizar como entrenamiento estén indicados en un fichero `train.txt` y los sets de validación en `test.txt`.

**Importante**: Las clases utilizadas para generar estos ficheros deben coincidir con las utilizadas para generar las etiquetas. Para especificar las clases, usar el parámetro `-C`.

``` sh
python gen_labels.py  -d <fichero_xml> -C 7 12 17 -c
```

A la hora de entrenar la red neuronal, se pueden seleccionar varios niveles de complejidad. A mayor complejidad, más tiempo es necesario para el entrenamiento y la detección de objetos, pero sobre todo incrementa la demanda de VRAM del modelo. Se puede indicar el nivel de complejidad deseado mediante el parámetro `-S` selecionando el nivel con una de las siguientes opciones (de menor a mayor complejidad): `n`, `s`, `m`, `l` o `x`. Por defecto el nivel de complejidad es `l`.

``` sh
python gen_labels.py  -d <fichero_xml> -C 7 12 17 -S l -c
```

**Nota**: El fichero de configuración generado añade una letra al final del nombre para indicar el nivel de complejidad del modelo.

**Consejo**: A la hora de generar los ficheros, es importante que el orden de generación y las clases especificadas sean coherentes. Para garantizar esto se recomienda generar todos los ficheros simultáneamente en una unica llamada:

``` sh
python gen_labels.py  -d <fichero_xml> -C 7 12 17 -S l -t 0.8 -lc
```

**Nota**: La ruta de los ficheros cambia según nos encontremos en nuestro equipo o dentro de la sesión interactiva de Docker. Es necesario regenerar los ficheros con `gen_labels.py` cada vez que se cambie de un entorno a otro.

Tras generar todos los ficheros (etiquetas, configuración y dataset), la estructura del directorio debería ser:

```
PROJ
 |
 - Tests
    |
    - 22009 Azh [con etiquetas junto a las imágenes]
    - Azahar 21334 [con etiquetas junto a las imágenes]
    - Azahar_21181 [con etiquetas junto a las imágenes]
    - ...
 - results
    |
    - ...
 - polenet-yolov5
    |
    - README.md
    - requirements.txt
    - gen_conf_mat.py
    - gen_labels.py
    - utils.py
    - polenet.yaml
    - polenet-yolov5l.yaml
```

A partir de este punto ya se tiene todo listo para trabajar con YOLOv5.
