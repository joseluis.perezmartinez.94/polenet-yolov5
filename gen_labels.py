"""Polenet Dataset Manager for YOLOv5

    * Author:  José Luis Pérez Martínez
    * Company: Universitat Politècnica de València
    * Date:    25/02/2022

This script offers a set of functionalities for managing and preparing
the Polenet dataset for working with YOLOv5.

This script requires the library `PyYAML` installed within the
environment you are running this script in.

This file can also be imported as a module and contains the following
functions:

    * getData - returns the summary of the dataset's labelled classes
    * showData - prints a summary of the dataset's labelled classes
    * createTrainTestList - generates the train ant test subsets' files
    * createCNNConf - generates the CNN configuration YAML file
    * createDataConf - generates the dataset configuration YAML file
    * createLabels - generates the label files for the dataset
    * main - the main function of the script
"""

import os, sys
import argparse
import yaml
import xml.etree.ElementTree as ET


def getData(xml_list):
    """Gets the summary of the labelled classes of the dataset
    
    Parameters
    ----------
    xml_list : list
        The list of XML files that compose the dataset
    
    Returns
    -------
    pollen_count : dict
        Dictionary of class ids (key) and its number of ocurrences
        (value) within the dataset
    """
    
    pollen_collection = dict()
    pollen_count = dict()

    # Check every XML for pollen labelled classes
    for xml in xml_list:
        tree_1 = ET.parse(xml)
        # Each XML may have several microscope slides
        for slide in tree_1.getroot().iter('microscopeSlides'):
            # Each microscope slide has several honey images
            for honey in slide.iter('honeyImages'):
                # Each honey image has some pollen samples labelled
                for pollen in honey.iter('pollenSamples'):
                    pollen_id = pollen.find('pollenID').text
                    pollen_name = pollen.find('pollenName').text
                    pollen_collection[int(pollen_id)] = pollen_name.strip()
                    pollen_count[int(pollen_id)] = pollen_count.setdefault(int(pollen_id), 0) + 1
                    
    return pollen_collection, pollen_count


def showData(xml_list, sort=False):
    """Prints the summary of the labelled classes of the dataset
    
    Parameters
    ----------
    xml_list : list
        The list of XML files that compose the dataset
    sort : bool, optional
        A flag used to indicate wether the printed list of classes
        should be orderer by class id (False) of by number of
        ocurrences (True). Default is False.
    
    Returns
    -------
    """
    
    # Get the data from the dataset
    pollen_collection, pollen_count = getData(xml_list)

    if sort:
        # Sort the classes by the number of ocurrences
        for k, v in sorted(pollen_count.items(), key=lambda x: x[1], reverse=True):
            print('[{id:3}] {name}: {count}'.format(name = pollen_collection[k], id = k, count = v))
    else:
        # Sort the classes by class ID
        for k, v in sorted(pollen_count.items()):
            print('[{id:3}] {name}: {count}'.format(name = pollen_collection[k], id = k, count = v))


def createTrainTestList(xml_list, ratio=0.9):
    """Generates the files with train and test subset of the dataset
    
    For each microscope slide in each of the XML in `xml_list`, the
    images are splitted in two groups: train and test. The images
    assigned to each group are determied as:
    
    * train: the first n images of the microscope slide, where:
        n = `ratio` * <num_microscope_slide_images>
    * test: the last m images of the microscope slide, where:
        m = (1 - `ratio`) * <num_microscope_slide_images>
        
    For example, consider 2 XML files:
        * xml_1 with 1 microscope slide of 40 images
        * xml_2 with 2 microscope slides of 10 and 30 images
    
    Assuming the default ratio of 0.9, the train subset will be
    composed of 72 (90%) from the total 80 images:
        * xml_1, slide_1, images from 1 to 36
        * xml_2, slide_1, images from 1 to 9
        * xml_2, slide_2, images from 1 to 27
        
    While the test subset will have the remaining 8 (10%):
        * xml_1, slide_1, images from 37 to 40
        * xml_2, slide_1, image 10
        * xml_2, slide_2, images from 28 to 30
    
    The images assigned to each subset will be saved as `train.txt`
    and `test.txt`
        
    ATTENTION: This function will only take into account images which
    have an associated label file. This function should be used after
    `createLabels` to ensure the expected results.
    
    Parameters
    ----------
    xml_list : list
        The list of XML files that compose the dataset
    ratio : float [0, 1], optional
        Ratio of the dataset. Default is 0.9.
    
    Returns
    -------
    """
    
    image_files_train = list()
    image_files_test = list()

    # Generate image list
    for xml in xml_list:
        tree_1 = ET.parse(xml)
        # Each XML may have several microscope slides
        for slide in tree_1.getroot().iter('microscopeSlides'):
            img_path = os.path.abspath(os.path.join(os.path.dirname(xml), slide.find('slideName').text))
            image_files_tmp = list()
            # Each microscope slide has several honey images
            for im_file in slide.iter('honeyImages'):
                im_file_name = im_file.find('imageName').text
                if os.path.isfile(os.path.join(img_path, im_file_name[:-4] + '.txt')):
                    image_files_tmp.append(os.path.join(img_path, im_file_name))
            train_limit = int(len(image_files_tmp)*ratio)
            image_files_train.extend(image_files_tmp[:train_limit])
            image_files_test.extend(image_files_tmp[train_limit:])

    with open('train.txt', 'w') as f:
        for im in image_files_train:
            f.write(im)
            f.write('\n')
            
    with open('test.txt', 'w') as f:
        for im in image_files_test:
            f.write(im)
            f.write('\n')


def createCNNConf(num_classes, size='l'):
    """Generates a YAML with the configuration of the cnn_size
    
    Generates a YAML with the CNN configuration parameters required
    by YOLOv5, including number of classes, size of the net, anchors,
    backbone and heads.
    
    The configuration is saved into 'polenet-YOLOv5<size>.yaml'.
    
    Parameters
    ----------
    num_classes : int
        Number of classes of the dataset
    size : str, optional
        Size of the CNN. Options, from smaller to larger: 'n', 's',
        'm', 'l' and 'x'. Default is 'l'. 
    
    Returns
    -------
    """
    
    # Select the CNN size
    depth_multiple = 1
    width_multiple = 1
    if size == 'n':
        depth_multiple = 0.33
        width_multiple = 0.25
    elif size == 's':
        depth_multiple = 0.33
        width_multiple = 0.50
    elif size == 'm':
        depth_multiple = 0.67
        width_multiple = 0.75
    elif size == 'l':
        depth_multiple = 1.0
        width_multiple = 1.0
    elif size == 'x':
        depth_multiple = 1.33
        width_multiple = 1.25
    
    # Config file
    conf =\
"""
# Parameters
nc: {nc}  # number of classes
depth_multiple: {dm}  # model depth multiple
width_multiple: {wm}  # layer channel multiple
anchors:
  - [10,13, 16,30, 33,23]  # P3/8
  - [30,61, 62,45, 59,119]  # P4/16
  - [116,90, 156,198, 373,326]  # P5/32

# YOLOv5 v6.0 backbone
backbone:
  # [from, number, module, args]
  [[-1, 1, Conv, [64, 6, 2, 2]],  # 0-P1/2
   [-1, 1, Conv, [128, 3, 2]],  # 1-P2/4
   [-1, 3, C3, [128]],
   [-1, 1, Conv, [256, 3, 2]],  # 3-P3/8
   [-1, 6, C3, [256]],
   [-1, 1, Conv, [512, 3, 2]],  # 5-P4/16
   [-1, 9, C3, [512]],
   [-1, 1, Conv, [1024, 3, 2]],  # 7-P5/32
   [-1, 3, C3, [1024]],
   [-1, 1, SPPF, [1024, 5]],  # 9
  ]

# YOLOv5 v6.0 head
head:
  [[-1, 1, Conv, [512, 1, 1]],
   [-1, 1, nn.Upsample, [None, 2, 'nearest']],
   [[-1, 6], 1, Concat, [1]],  # cat backbone P4
   [-1, 3, C3, [512, False]],  # 13

   [-1, 1, Conv, [256, 1, 1]],
   [-1, 1, nn.Upsample, [None, 2, 'nearest']],
   [[-1, 4], 1, Concat, [1]],  # cat backbone P3
   [-1, 3, C3, [256, False]],  # 17 (P3/8-small)

   [-1, 1, Conv, [256, 3, 2]],
   [[-1, 14], 1, Concat, [1]],  # cat head P4
   [-1, 3, C3, [512, False]],  # 20 (P4/16-medium)

   [-1, 1, Conv, [512, 3, 2]],
   [[-1, 10], 1, Concat, [1]],  # cat head P5
   [-1, 3, C3, [1024, False]],  # 23 (P5/32-large)

   [[17, 20, 23], 1, Detect, [nc, anchors]],  # Detect(P3, P4, P5)
  ]
""".format(nc=num_classes, dm=depth_multiple, wm=width_multiple)
    
    with open('polenet-YOLOv5' + size + '.yaml', 'w') as f:
        f.write(conf)


def createDataConf(xml_list, classes=None):
    """Generates a YAML with the configuration of the dataset
    
    Generates a YAML with the dataset parameters required
    by YOLOv5, including the path to the training and validation
    datasets, the number of classes and the name of the classes.
    
    The configuration is saved into 'polenet.yaml'.
    
    Parameters
    ----------
    xml_list : list
        The list of XML files that compose the dataset.
    classes : list, optional
        List of IDs of the classes to be included in the model, either
        for training, validation or detection. If no classes are
        specified, the model will include all the classes found in the
        dataset.
    
    Returns
    -------
    int
        Number of classes
    """
    
    pollen_collection, pollen_count = getData(xml_list)
    
    class_names = list()
    if not (classes is None):
        for i in sorted(classes):
            class_names.append(pollen_collection[i])
    else:
        last_tag = sorted(pollen_collection.keys())[-1]
        class_names = [pollen_collection.get(k, 'Class_{0}'.format(k)) for k in range(last_tag + 1)]
    
    conf = {
        # Train/val/test sets as 1) dir: path/to/imgs, 2) file: path/to/imgs.txt, or 3) list: [path/to/imgs1, path/to/imgs2, ..]
        'path': os.getcwd(),
        'train': 'train.txt',
        'val': 'test.txt',
        #'test': 'test.txt',
        
        # Classes
        'nc': len(class_names),
        'names': class_names,
    }

    with open('polenet.yaml', 'w') as f:
        yaml.dump(conf, f, allow_unicode=True)
    
    return len(class_names)


def createLabels(xml_list, classes=None, image_w=2464, image_h=2056):
    """Generates the labels of the dataset
    
    Generates one `.txt` file per image with the information of the
    labelled classes within it. According with the requirements of
    YOLOv5, the labels will have the same name that the image they
    refer to and will be placed into the same directory that the image
    itself.
    
    "path/to/image.jpg" -> "path/to/image.txt"
    
    The labels are formatted according to the YOLOv5 standard:
    
    "<class_id> <x_center_bb> <y_center_bb> <bb_width> <bb_height>"
    
    where `x_center_bb`, `y_center_bb`, `bb_width` and `bb_height` are
    normalized with the image width and height.
    
    The labels are generated only if the image has at least 1 of the
    classes specified in `classes`.
    
    Parameters
    ----------
    xml_list : list
        The list of XML files that compose the dataset.
    classes : list, optional
        List of IDs of the classes to be included in the model, either
        for training, validation or detection. If no classes are
        specified, the model will include all the classes found in the
        dataset.
    image_w : int, optional
        Width of the images
    image_h : int, optional
        Height of the images
    
    Returns
    -------
    """
    
    cwd = os.getcwd()
    img_dir = os.path.join(cwd, 'Tests')
    
    class_map = dict()
    if classes:
        for i, v in enumerate(sorted(classes)):
            class_map[v] = i

    # Check every XML
    for xml in xml_list:
        tree_1 = ET.parse(xml)
        # Each XML may have several microscope slides
        for slide in tree_1.getroot().iter('microscopeSlides'):
            img_path = os.path.join(os.path.dirname(xml), slide.find('slideName').text)
            # Each microscope slide has several honey images
            for honey in slide.iter('honeyImages'):
                img = honey.find('imageName').text[:-4]
                added_pollen = False
                with open(os.path.join(img_path, img + '.txt'), 'w') as f:
                    # Each honey image has some pollen samples labelled
                    for pollen in honey.iter('pollenSamples'):
                        pollen_id = int(pollen.find('pollenID').text)
                        if classes:
                            if pollen_id in classes:
                                added_pollen = True
                                pollen_x = float(pollen.find('X').text)
                                pollen_y = float(pollen.find('Y').text)
                                pollen_size = float(pollen.find('pollenSize').text)
                                f.write('{c} {x} {y} {w} {h}\n'.format( c = class_map[pollen_id],
                                                                        x = pollen_x/image_w,
                                                                        y = pollen_y/image_h,
                                                                        w = pollen_size/image_w,
                                                                        h = pollen_size/image_h))
                        else:
                            added_pollen = True
                            pollen_x = float(pollen.find('X').text)
                            pollen_y = float(pollen.find('Y').text)
                            pollen_size = float(pollen.find('pollenSize').text)
                            f.write('{c} {x} {y} {w} {h}\n'.format( c = pollen_id,
                                                                    x = pollen_x/image_w,
                                                                    y = pollen_y/image_h,
                                                                    w = pollen_size/image_w,
                                                                    h = pollen_size/image_h))
                # If the image has not pollen samples of the provided `classes`, remove the created file
                if not added_pollen:
                    os.remove(os.path.join(img_path, img + '.txt'))


def main(args):
    """Main function of the script
    
    Parameters
    ----------
    args : list
        List of input parameters and flags
    
    Returns
    -------
    """
    
    parser = argparse.ArgumentParser(
    description=__doc__,
    formatter_class=argparse.RawTextHelpFormatter)
    
    parser.add_argument(
        '-d', '--datasets',
        metavar='XML',
        nargs='+',
        required=True,
        help='One or more XML files with the description of a Polenet dataset.')
    
    parser.add_argument(
        '-C', '--classes',
        metavar='CLASS',
        nargs='+',
        type=int,
        help='Polenet classes (ID) to be used in the model.')
    
    parser.add_argument(
        '-S', '--cnn-size',
        metavar='SIZE',
        default='l',
        choices=['n', 's', 'm', 'l', 'x'],
        help='Polenet classes (ID) to be used in the model.')

    parser.add_argument(
        '-l', '--gen-labels',
        action='store_true',
        help='Generates YOLOv5 labels for the provided classes from the selected datasets. If no classes are provided explicitely, all the classes found on the datasets will be used.')
    
    parser.add_argument(
        '-c', '--gen-config',
        action='store_true',
        help='Generates YOLOv5 labels for the provided classes from the selected datasets. If no classes are provided explicitely, all the classes found on the datasets will be used.')
    
    parser.add_argument(
        '-t', '--train-test',
        metavar='RATIO',
        nargs='?',
        default=None,
        const=0.9,
        type=float,
        help='Generates a "train.txt" and a "test.txt" files. The images from Polenet are distributed between these two files. The ratio of images provided to each set is defined by the parameter RATIO, where:\n - num_images * RATIO images are assigned to "train.txt"\n - num_images * (1-RATIO)  images are assigned to "test.txt"\nRATIO is expected in the range [0, 1]. By default RATIO = 0.9.')

    parser.add_argument(
        '-s',
        '--show',
        metavar='sorted',
        nargs='?',
        const='',
        choices=['', 'sorted'],
        help='Show a summary of the labels at the provided Polenet datasets. If specified "sorted", the labels will be orderer by ocurrence.')
    
    args = parser.parse_args()
    
    dataset_list = list()
    for d in args.datasets:
        if d.endswith('.xml'):
            dataset_list.append(d)
        elif d.endswith('.txt'):
            with open(d) as f:
                for line in f:
                    line = line.strip()
                    if line.endswith('.xml'):
                        file_path = os.path.dirname(d)
                        dataset_list.append(os.path.join(file_path, line.strip()))
    
    if not (args.show is None):
        showData(dataset_list, sort=(args.show=='sorted'))
    if args.gen_labels:
        sys.stdout.write('Generating labels...')
        createLabels(dataset_list, classes=args.classes)
        print(' Done')
    if args.gen_config:
        sys.stdout.write('Generating Polenet config file...')
        num_classes = createDataConf(dataset_list, classes=args.classes)
        print(' Done')
        sys.stdout.write('Generating YOLOv5 config file...')
        createCNNConf(num_classes, size=args.cnn_size)
        print(' Done')
    if not (args.train_test is None):
        sys.stdout.write('Generating train/test files...')
        createTrainTestList(dataset_list, ratio=max(0, min(1, args.train_test)))
        print(' Done')

    
if __name__ == '__main__':
    main(sys.argv[1:])

